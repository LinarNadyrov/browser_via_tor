#!/bin/bash
# run X virtual with parameters
Xvfb :1 -screen 0 1280x800x16 &
/usr/bin/x11vnc -forever -usepw -create -display :1.0 -usepw &
# parameter to display the item in X11
DISPLAY=:1.0
# variable environment
export DISPLAY
firefox

