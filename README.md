# browser_via_tor

Запуск браузера через tor.

1. Клонируем репозиторий 

```bash
git clone https://gitlab.com/LinarNadyrov/browser_via_tor.git
```
2. Переходим в каталог
```bash
cd browser_via_tor/
``` 
3. Запускаем контейнеры (запуск после сборки)
```bash
docker-compose up -d
```
4. Содержимое docker-compose.yml 

```yaml
services:
  browser:
    build:
      context: ./browser
      dockerfile: Dockerfile
    restart: always
    container_name: browser
    depends_on:
      - proxy
    links:
      - "proxy:proxy"
    ports:
      - "5900:5900"
  proxy:
    build:
      context: ./proxy
      dockerfile: Dockerfile
    restart: always
    container_name: proxy
``` 
5. Проверка работоспособности

а) В программе *Remmina Remote Desktop Client* создаем подключение\
Protocol - Remmina VNC Plugin\
Server   - localhost:5900 (если docker-compose up -d выполнялся локально)\
User password - 1234 (задается в ./browser/Dockerfile)

![](./image/remmina.png)

б) После того как подключились к браузеру проверяем ip адрес.\
Можно ввести например 2ip.ru

![](./image/check_ip_address.png)

6. Все, удачной работы :-)

----
Написать docker-compoe.yaml файл в котором будут подниматься виртуальные X, где будет работать хром (или другой браузер). Доступ к хрому должен осуществляться посредством vnc, порт которого должен быть доступен сразу после запуска контейнера/ов (порт может быть динамическим). Внутри контейнера  браузер должен работать через tor.\
В качестве результата принимается docker-compose.yaml файл с локальным билдом через dockkerfile, который достаточно запустить через docker-compose up -d без каких либо дополнительных настроек (можно ссылку на гитхаб).\
P.S  если очень хочется можно сделать через kubernetes.yaml будет бонусом